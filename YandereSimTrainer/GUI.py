import dearpygui.dearpygui as dpg
import UnityHackYandere as yandere
import os
def StartDPG():
    dpg.create_context()
    with dpg.font_registry():
        nimbus_sans_l = dpg.add_font(os.getcwd() + "\\Setting\\NimbusSans.otf", 15)
    dpg.bind_font(nimbus_sans_l)
    with dpg.window(tag="YANDERESIMULATOR"):
        dpg.add_text("This Is My First Trainer For Game Yandere Simulator", color=[70, 70, 130])
        dpg.add_checkbox(label="Unlimited Points(Panty Shots)", callback=lambda : yandere.UnityYandere.SetPantyShots())
        dpg.add_button(label="Allocate Console", callback=yandere.UnityYandere.AllocateNewConsole)
    dpg.create_viewport(title='Yandere Simulator Trainer by DarknessSoul', width=755, height=755)
    dpg.setup_dearpygui()
    dpg.show_viewport()
    dpg.set_primary_window("YANDERESIMULATOR", True)
    dpg.start_dearpygui()
    dpg.destroy_context()